using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29_9
{
    class Program
    {
        static void Main()
        {
            reversal();
        }
        static void reversal()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int revers = 0;
            int t;
            while(n > 0)
            {
                t = n % 10;
                revers = 10 * revers + t;
                n = n/ 10;

            }
            Console.WriteLine(revers);
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] numbers = new int[5, 2];
            for(int i = 0; i < 5; i++)
            {
                for(int j = 0; j < 2; j++)
                {
                   numbers[i,j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            int min = numbers[0, 0];
            for(int i = 0; i < 5; i++)
            {
                for(int j=0 ; j<2; j++)
                {
                    if(numbers[i,j] < min)
                    {
                        min = numbers[i, j];
                    }
                }
            }

            Console.WriteLine("min : " + min);
        }
    }
}

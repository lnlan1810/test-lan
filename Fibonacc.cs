using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trai_tim
{
    class fibonacci
    {
        static void Main()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] Array = new int[n, n];

           // print the Fibonacci sequence in the first way
            int a = 0;
                for (int i = 0; i < n; i++)
                {
                 for (int j = 0; j < n; j++) 
                    {
                    
                    Console.Write(Fibonacci(a) + " ");
                    a++;
                    }
                
                    Console.WriteLine();
               
                }

            Console.WriteLine();

            // print the Fibonacci sequence in the second way
            int b = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {

                    Console.Write(Fibonacci1(b) + " ");
                    b++;
                }

                Console.WriteLine();

            }
        }

        // function to print the sequence of Fibonacci numbers using loop
        static int Fibonacci(int n)
        {

            int f0 = 0;
            int f1 = 1;
            int fn = 1;
            if(n < 0)
            {
                return -1;
            }
            else if (n == 1 || n == 2)
            {
                return n;
            }
            else
            {
                for (int i = 2; i < n; i++)
                {

                    f0 = f1;
                    f1 = fn;
                    fn = f0 + f1;
                }
                return fn;
            }
        }

        // function to print Fibonacci sequence by recursion
        static int Fibonacci1(int num)
        {
            if (num == 0)
                return 0;
            else if (num == 1)
            {
                return 1;
            }
            else
                return (Fibonacci1(num - 1) + Fibonacci1(num - 2));
        }
    }
    
}
